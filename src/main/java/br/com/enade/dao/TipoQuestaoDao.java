package br.com.enade.dao;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import br.com.enade.model.Tbtipoquestao;
public class TipoQuestaoDao implements Serializable {
	private DAO<Tbtipoquestao> dao;

	@PostConstruct


	public Tbtipoquestao buscarPorId(Long tipoQuestaoId) {
		return this.dao.buscaPorId(tipoQuestaoId);
	}

}
