package br.com.enade.dao;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import br.com.enade.model.Tbtipousuario;

public class TipoUsuarioDao implements Serializable {


	private DAO<Tbtipousuario> dao;

	@PostConstruct


	public Tbtipousuario buscarPorId(Long tipoUsuarioId) {
		return this.dao.buscaPorId(tipoUsuarioId);
	}



}
