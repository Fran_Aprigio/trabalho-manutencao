package br.com.enade.bean;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import br.com.enade.dao.UsuarioDao;
import br.com.enade.model.Tbusuario;

@Named
@SessionScoped
public class LoginBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Tbusuario usuario;

	@Inject
	UsuarioDao dao;

	@Inject
	FacesContext context;

	private Tbusuario usuarioLogado;

	public Tbusuario getUsuario() {
		return usuario;
	}


	
	public Tbusuario getUsuarioLogado() {
		return usuarioLogado;
	}

}
